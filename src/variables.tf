variable "api_gateway_name" {
  type = string
}

variable "domain_name" {
  type = string
}

variable "oidc_client_id" {
  type = string
}

variable "oidc_client_secret" {
  type = string
}

variable "provider_name" {
  type = string
}

variable "oidc_issuer" {
  type = string
}

variable "oidc_url" {
  type = string
}

variable "app_client_subject" {
  type = string
}

variable "header_subject" {
  type = string
}

variable "token_lambda_name" {
  type = string
}

variable "auth_lambda_name" {
  type = string
}

variable "realm_lambda_name" {
  type = string
}

variable "aws_account_id" {
  type = string
}

variable "profile" {
  type = string
}

variable "secret_manager_id" {
  type = string
}

variable "region" {
  type = string
}