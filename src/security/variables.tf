variable "domain_name" {
  type = string
}

variable "oidc_client_id" {
  type = string
}

variable "oidc_client_secret" {
  type = string
}

variable "provider_name" {
  type = string
}

variable "oidc_issuer" {
  type = string
}