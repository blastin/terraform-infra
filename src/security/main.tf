resource "aws_cognito_user_pool" "user_pool" {
  name = var.domain_name
}

resource "aws_cognito_user_pool_domain" "domain" {
  domain       = var.domain_name
  user_pool_id = aws_cognito_user_pool.user_pool.id
}

resource "aws_cognito_resource_server" "resource" {
  identifier = var.domain_name
  name       = var.domain_name
  scope {
    scope_name        = "consumo"
    scope_description = "Consome informações de recurso de maquina"
  }
  user_pool_id = aws_cognito_user_pool.user_pool.id
}

resource "aws_cognito_user_pool_client" "user_pool_credentials_client" {
  name                                 = "client"
  user_pool_id                         = aws_cognito_user_pool.user_pool.id
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_flows                  = ["client_credentials"]
  supported_identity_providers         = ["COGNITO"]
  generate_secret                      = true
  allowed_oauth_scopes                 = ["${aws_cognito_resource_server.resource.name}/consumo"]
}

resource "aws_cognito_user_pool_client" "user_pool_authorize_client" {
  depends_on                           = [aws_cognito_resource_server.resource, aws_cognito_identity_provider.keycloak]
  name                                 = "authorize"
  user_pool_id                         = aws_cognito_user_pool.user_pool.id
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_flows                  = ["code"]
  supported_identity_providers         = [var.provider_name]
  generate_secret                      = false
  allowed_oauth_scopes                 = ["openid", "${aws_cognito_resource_server.resource.name}/consumo"]
  callback_urls                        = ["https://www.google.com"]
}

resource "aws_cognito_identity_provider" "keycloak" {

  user_pool_id = aws_cognito_user_pool.user_pool.id

  provider_name = var.provider_name

  provider_type = "OIDC"

  provider_details = {
    authorize_scopes          = "openid"
    client_id                 = var.oidc_client_id
    client_secret             = var.oidc_client_secret
    oidc_issuer               = var.oidc_issuer
    attributes_request_method = "GET"
  }
}

output "client_id" {
  value = aws_cognito_user_pool_client.user_pool_credentials_client.id
}

output "client_secret" {
  value = aws_cognito_user_pool_client.user_pool_credentials_client.client_secret
}
