module "auth" {
  source           = "./auth"
  api_gateway_id   = module.api.api_gateway_id
  domain_name      = var.domain_name
  auth_lambda_name = var.auth_lambda_name
  aws_account_id   = var.aws_account_id
}

module "realm" {
  source            = "./realm"
  api_gateway_id    = module.api.api_gateway_id
  oidc_url          = var.oidc_url
  realm_lambda_name = var.realm_lambda_name
  aws_account_id    = var.aws_account_id
}

module "token" {
  source             = "./token"
  api_gateway_id     = module.api.api_gateway_id
  app_client_subject = var.app_client_subject
  header_subject     = var.header_subject
  token_lambda_name  = var.token_lambda_name
  aws_account_id     = var.aws_account_id
}

module "api" {
  source           = "./api"
  api_gateway_name = var.api_gateway_name
  token_invoke_arn = module.token.token_invoke_arn
  auth_invoke_arn  = module.auth.auth_invoke_arn
  realm_invoke_arn = module.realm.realm_invoke_arn
}

module "security" {
  source             = "./security"
  domain_name        = var.domain_name
  oidc_client_id     = var.oidc_client_id
  oidc_client_secret = var.oidc_client_secret
  provider_name      = var.provider_name
  oidc_issuer        = "${var.oidc_url}/${var.oidc_issuer}"
}

module "manager" {
  source            = "./manager"
  client_id         = module.security.client_id
  client_secret     = module.security.client_secret
  domain_name       = var.domain_name
  secret_manager_id = var.secret_manager_id
  aws_account_id    = var.aws_account_id
}
