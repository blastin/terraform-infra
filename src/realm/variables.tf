variable "api_gateway_id" {
  type = string
}

variable "realm_lambda_name" {
  type = string
}

variable "aws_account_id" {
  type = string
}

variable "oidc_url"{
  type = string
}