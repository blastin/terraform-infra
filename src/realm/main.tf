resource "random_id" "id_lambda" {
  keepers = {
    "ami_id" = "abcdefghijklmnopqrstuvwxyz1234567890_"
  }
  byte_length = 8
}

resource "aws_iam_role" "iam_for_realm_lambda" {
  name               = "iam-${random_id.id_lambda.hex}"
  assume_role_policy = file("../resources/realm/role.json")
}

resource "aws_iam_policy" "realm_lambda_logging" {
  name        = "realm_lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"
  policy      = file("../resources/realm/policy.json")
}

resource "aws_iam_role_policy_attachment" "realm_lambda_logs" {
  role       = aws_iam_role.iam_for_realm_lambda.name
  policy_arn = aws_iam_policy.realm_lambda_logging.arn
}

data "archive_file" "realm_lambda_zip" {
  type             = "zip"
  source_file      = "../resources/realm/index.js"
  output_file_mode = "0666"
  output_path      = ".terraform/bin/realm/index.zip"
}

resource "aws_lambda_function" "realm_lambda" {
  filename         = data.archive_file.realm_lambda_zip.output_path
  function_name    = var.realm_lambda_name
  role             = aws_iam_role.iam_for_realm_lambda.arn
  handler          = "index.handler"
  source_code_hash = data.archive_file.realm_lambda_zip.output_base64sha256
  runtime          = "nodejs18.x"
  environment {
    variables = {
      URL = var.oidc_url
    }
  }
}

#TODO RECUPERAR SOURCE ARN PERMISSION LAMBDA
resource "aws_lambda_permission" "api-gateway" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.realm_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${var.aws_account_id}:${var.api_gateway_id}/*/*/realms/*"
}

output "realm_invoke_arn" {
  value = aws_lambda_function.realm_lambda.invoke_arn
}
