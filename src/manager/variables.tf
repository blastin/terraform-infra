variable "domain_name" {
  type = string
}

variable "client_id" {
  type = string
}

variable "client_secret" {
  type = string
}

variable "secret_manager_id" {
  type = string
}

variable "aws_account_id"{
  type = string
}