resource "aws_secretsmanager_secret_version" "example" {
  secret_id     = "arn:aws:secretsmanager:${var.aws_account_id}:secret:${var.secret_manager_id}"
  secret_string = <<EOF
  {
    "id": "${var.client_id}",
    "secret": "${var.client_secret}",
    "domain": "${var.domain_name}"
  }
EOF
}
