variable "api_gateway_id" {
  type = string
}

variable "domain_name" {
  type = string
}

variable "auth_lambda_name" {
  type = string
}

variable "aws_account_id" {
  type = string
}