resource "random_id" "id_lambda" {
  keepers = {
    "ami_id" = "abcdefghijklmnopqrstuvwxyz1234567890_"
  }
  byte_length = 8
}

resource "aws_iam_role" "iam_for_auth_lambda" {
  name               = "iam-${random_id.id_lambda.hex}"
  assume_role_policy = file("../resources/auth/role.json")
}

resource "aws_iam_policy" "auth_lambda_logging" {
  name        = "auth_lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"
  policy      = file("../resources/auth/policy.json")
}

resource "aws_iam_role_policy_attachment" "auth_lambda_logs" {
  role       = aws_iam_role.iam_for_auth_lambda.name
  policy_arn = aws_iam_policy.auth_lambda_logging.arn
}

data "archive_file" "auth_lambda_zip" {
  type             = "zip"
  source_file      = "../resources/auth/index.js"
  output_file_mode = "0666"
  output_path      = ".terraform/bin/auth/index.zip"
}

resource "aws_lambda_function" "auth_lambda" {
  filename         = data.archive_file.auth_lambda_zip.output_path
  function_name    = var.auth_lambda_name
  role             = aws_iam_role.iam_for_auth_lambda.arn
  handler          = "index.handler"
  source_code_hash = data.archive_file.auth_lambda_zip.output_base64sha256
  runtime          = "nodejs18.x"
  environment {
    variables = {
      DOMAIN_NAME = var.domain_name
    }
  }
}

#TODO RECUPERAR SOURCE ARN PERMISSION LAMBDA
resource "aws_lambda_permission" "api-gateway" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.auth_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${var.aws_account_id}:${var.api_gateway_id}/*/GET/authorize"
}

output "auth_invoke_arn" {
  value = aws_lambda_function.auth_lambda.invoke_arn
}
