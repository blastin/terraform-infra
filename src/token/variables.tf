variable "api_gateway_id" {
  type = string
}

variable "app_client_subject" {
  type = string
}

variable "header_subject" {
  type = string
}

variable "token_lambda_name" {
  type = string
}

variable "aws_account_id" {
  type = string
}