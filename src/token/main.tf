resource "random_id" "id_lambda" {
  keepers = {
    "ami_id" = "abcdefghijklmnopqrstuvwxyz1234567890_"
  }
  byte_length = 8
}

resource "aws_iam_role" "iam_for_token_lambda" {
  name               = "iam-${random_id.id_lambda.hex}"
  assume_role_policy = file("../resources/token/role.json")
}

resource "aws_iam_policy" "token_lambda_policy" {
  name        = "token_lambda_policy"
  path        = "/"
  description = "IAM policy for token lambda"
  policy      = file("../resources/token/policy.json")
}

resource "aws_iam_role_policy_attachment" "token_lambda_policy" {
  role       = aws_iam_role.iam_for_token_lambda.name
  policy_arn = aws_iam_policy.token_lambda_policy.arn
}

data "archive_file" "token_lambda_zip" {
  type             = "zip"
  source_file      = "../resources/token/index.js"
  output_file_mode = "0666"
  output_path      = "./.terraform/bin/token/index.zip"
}

resource "aws_lambda_function" "token_lambda" {
  filename         = data.archive_file.token_lambda_zip.output_path
  function_name    = var.token_lambda_name
  role             = aws_iam_role.iam_for_token_lambda.arn
  handler          = "index.handler"
  source_code_hash = data.archive_file.token_lambda_zip.output_base64sha256
  runtime          = "nodejs18.x"
  layers           = [aws_lambda_layer_version.fetch_node.arn]
  environment {
    variables = {
      APP_CLIENT_SUBJECT = var.app_client_subject
      HEADER_SUBJECT     = var.header_subject
    }
  }
}

#TODO RECUPERAR SOURCE ARN PERMISSION LAMBDA
resource "aws_lambda_permission" "api-gateway" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.token_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${var.aws_account_id}:${var.api_gateway_id}/*/POST/token"
}

resource "aws_lambda_layer_version" "fetch_node" {
  filename            = "../resources/token/modules/layer.zip"
  layer_name          = "node_fetch_v2"
  compatible_runtimes = ["nodejs18.x"]
}

output "token_invoke_arn" {
  value = aws_lambda_function.token_lambda.invoke_arn
}
