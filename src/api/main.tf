resource "aws_api_gateway_rest_api" "api" {
  name = var.api_gateway_name
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

################## AUTHORIZE ##################

resource "aws_api_gateway_resource" "authorize" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "authorize"
}

resource "aws_api_gateway_method" "authorize_method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.authorize.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "authorize_integration" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.authorize.id
  http_method             = aws_api_gateway_method.authorize_method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = var.auth_invoke_arn
}

################## REALMS ##################

resource "aws_api_gateway_resource" "realms_resource" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "realms"
}

resource "aws_api_gateway_resource" "realm_proxy_resource" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_resource.realms_resource.id
  path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "realm_proxy_method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.realm_proxy_resource.id
  http_method   = "ANY"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "realm_proxy_integration" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.realm_proxy_resource.id
  http_method             = aws_api_gateway_method.realm_proxy_method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = var.realm_invoke_arn
}

################## TOKEN ##################

resource "aws_api_gateway_resource" "token" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "token"
}

resource "aws_api_gateway_method" "token_method" {
  rest_api_id          = aws_api_gateway_rest_api.api.id
  resource_id          = aws_api_gateway_resource.token.id
  http_method          = "POST"
  authorization        = "NONE"
  request_models       = { "application/json" = "${aws_api_gateway_model.TokenModel.name}" }
  request_validator_id = aws_api_gateway_request_validator.request_validator.id
}

resource "aws_api_gateway_model" "TokenModel" {
  rest_api_id  = aws_api_gateway_rest_api.api.id
  name         = "TokenModel"
  content_type = "application/json"
  schema       = file("../resources/api/token_model.json")
}

resource "aws_api_gateway_request_validator" "request_validator" {
  name                        = "validator"
  rest_api_id                 = aws_api_gateway_rest_api.api.id
  validate_request_body       = true
  validate_request_parameters = true
}

resource "aws_api_gateway_integration" "token_integration" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.token.id
  http_method             = aws_api_gateway_method.token_method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = var.token_invoke_arn
}

################## ######### ##################

resource "aws_api_gateway_deployment" "api_deployment" {
  rest_api_id = aws_api_gateway_rest_api.api.id

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.api.body))
  }

  lifecycle {
    create_before_destroy = true
  }

  depends_on = [
    aws_api_gateway_integration.token_integration, aws_api_gateway_integration.authorize_integration
  ]
}

resource "aws_api_gateway_stage" "api_stage" {
  deployment_id = aws_api_gateway_deployment.api_deployment.id
  rest_api_id   = aws_api_gateway_rest_api.api.id
  stage_name    = "v1"
}


output "api_gateway_id" {
  value = aws_api_gateway_rest_api.api.id
}


########

resource "aws_api_gateway_account" "account" {
  cloudwatch_role_arn = aws_iam_role.cloudwatch.arn
}

resource "aws_iam_role" "cloudwatch" {
  name               = "api-gateway-${aws_api_gateway_rest_api.api.name}"
  assume_role_policy = file("../resources/api/role.json")
}

resource "aws_iam_role_policy" "cloudwatch" {
  name   = "default"
  role   = aws_iam_role.cloudwatch.id
  policy = file("../resources/api/policy.json")
}

resource "aws_api_gateway_method_settings" "general_settings" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name  = aws_api_gateway_stage.api_stage.stage_name

  method_path = "*/*"
  settings {
    # Enable CloudWatch logging and metrics
    metrics_enabled    = true
    data_trace_enabled = true
    logging_level      = "INFO"
    # Limit the rate of calls to prevent abuse and unwanted charges
    throttling_rate_limit  = 100
    throttling_burst_limit = 50
  }
  depends_on = [
    aws_api_gateway_account.account
  ]
}
