variable "api_gateway_name" {
  type = string
}

variable "token_invoke_arn" {
  type = string
}

variable "auth_invoke_arn" {
  type = string
}

variable "realm_invoke_arn" {
  type = string
}