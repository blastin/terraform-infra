exports.handler = async (event, _, callback) => {

    const client = {
        id: event.queryStringParameters["client_id"],
        scope: "openid",
        redirect: event.queryStringParameters["redirect_uri"],
        domain: process.env.DOMAIN_NAME
    };

    const query = `client_id=${client.id}&response_type=code&scope=${client.scope}&redirect_uri=${client.redirect}`;

    const location = `https://${client.domain}.auth.${process.env.AWS_REGION}.amazoncognito.com/oauth2/authorize?${query}`;

    const response = {

        statusCode: 301,
        headers: {
            Location: location,
        }
    };

    return callback(null, response);

};
