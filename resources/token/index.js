const fetch = require('node-fetch');

const sm = require('@aws-sdk/client-secrets-manager');

const APP_CLIENT = process.env.APP_CLIENT_SUBJECT;

const HEADER_SUBJECT = process.env.HEADER_SUBJECT;

function parse_client(client_sub) {

    const express = client_sub
        .concat(",")
        .replace(/\s+/g, '')
        .replace(/=/g, ":")
        .replace(/(\w[^:]*)(?=,)/gi, '"$1"')
        .replace(/(['"])?(\w+)(['"])?:/g, '"$2": ');

    const json = express.substring(',', express.length - 1);

    const object = JSON.parse(`{${json}}`);

    return object;

}

async function create_client(client_sub) {

    let subject

    try {

        subject = parse_client(client_sub)[APP_CLIENT];

    } catch (error) {

        throw response_error
            (
                `Subject('${client_sub}') is not a valid client credential`,
                {
                    message: `Client certificate subject(${client_sub}) field is not valid. Possible misconfiguration in MTLS server`,
                },
                400
            );

    }

    const secret_name = `client_credentials/${subject}`;

    const client = new sm.SecretsManagerClient({ region: process.env.AWS_REGION });

    return client.send(

        new sm.GetSecretValueCommand({
            SecretId: secret_name,
            VersionStage: "AWSCURRENT"
        })

    )
        .then(response => JSON.parse(response.SecretString))
        .then(credentials => {

            return {
                domain: credentials.domain,
                id: credentials.id,
                secret: credentials.secret,
                name: secret_name
            }

        }

        ).catch(error => {

            throw response_error
                (
                    `Client('${subject}') not authorized to complete request`,
                    {
                        message: error.message,
                        requestId: error["$metadata"].requestId
                    },
                    403
                );
        });

}

function response_error(message, description, statusCode) {

    return {

        body: JSON.stringify({
            message: message,
            description: description
        }),
        statusCode: statusCode
    };
}

function response_cognito_error(client, requestId, stack) {

    return response_error
        (
            `Unable to complete request due to internal error`,
            {
                message: `Invalid secret-manager[${client.name}}] internal parameters, unable to generate token`,
                requestApiGatewayId: requestId,
                stack: stack
            },
            422
        );

}

async function create_token(client, requestId) {

    return fetch(`https://${client.domain}.auth.${process.env.AWS_REGION}.amazoncognito.com/oauth2/token`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: `grant_type=client_credentials&client_id=${client.id}&client_secret=${client.secret}`
        }
    )
        .then(res => Promise.all([res.status, res.json()]))
        .then(([status, jsonData]) => {

            if (status == 400) {
                return response_cognito_error(client, requestId, jsonData)
            }

            return {
                body: JSON.stringify(jsonData),
                statusCode: status
            };


        }).catch(error => response_cognito_error(client, requestId, error));

}

exports.handler = async (event) => {

    try {
        const client = await create_client(event.headers[HEADER_SUBJECT]);
        return create_token(client, event.requestContext.requestId);
    } catch (error) {
        return error
    }

};
