#!/bin/bash

envsubst "$(printf '${%s} ' $(env | cut -d'=' -f1))" < /tmp/nginx/proxy.conf > /etc/nginx/conf.d/default.conf
#
nginx -g 'daemon off;'