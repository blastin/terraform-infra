#!/bin/bash

URL_TOKEN="https://internal.server:443/$1"

ACCESS_TOKEN="$(curl -v -X POST -H "Host:internal.server" --resolve "internal.server:443:127.0.0.1" \
    --cacert certs/ca.crt --key certs/client.key --cert certs/client.crt \
    --location "${URL_TOKEN}" | jq '.access_token')"

if [[ "$ACCESS_TOKEN" =~ \w+ ]]; then
    export AWS_PROFILE="default"
    HOST="localhost:443"
    USER_POOL_ID="$(aws cognito-idp list-user-pools --query "UserPools[?Name=='knin-project-online'].Id |[0]" --max-results 20 | tr -d \")"
    CLIENT_ID="$(aws cognito-idp list-user-pool-clients --user-pool-id "$USER_POOL_ID" --query "UserPoolClients[?ClientName=='authorize'].ClientId | [0]" | tr -d \")"
    URL="https://$HOST/authorize?client_id=$CLIENT_ID&redirect_uri=https://www.google.com"
    xdg-open "$URL"
else
    echo "Não foi possivel concluir teste"
fi
