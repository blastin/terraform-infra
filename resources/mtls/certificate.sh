#!/bin/bash

DOMINIO=${1:-"knin-project.online"}
PASTA=${2:-"certs"}

mkdir -p "$PASTA"

# # CREATE KEY ROOT
# openssl genrsa -out "$PASTA/$DOMINIO.key" 2048

# #CRIANDO CERTIFICADO DE AUTORIZAÇÃO
# openssl req -new -x509 -days 3650 -subj "/C=BR/ST=SP/CN=$DOMINIO" -key "$PASTA/$DOMINIO.key" -out "$PASTA/$DOMINIO.crt"
# cat "$PASTA/$DOMINIO.crt" > "$PASTA/ca.crt"

# #CRIANDO CERTIFICADO INTERMEDIARIO
# INTERMEDIARIO="$PASTA"
# mkdir -p "$INTERMEDIARIO"
# openssl req -out "$INTERMEDIARIO/server.cs"r -newkey rsa:2048 -nodes -keyout "$INTERMEDIARIO/server.key" -subj "/CN=internal.server"
# openssl x509 -req -sha256 -days 365 -CA "$PASTA/$DOMINIO.crt" -CAkey "$PASTA/$DOMINIO.key" -set_serial 0 -in "$INTERMEDIARIO/server.csr" -out "$INTERMEDIARIO/server.crt"

# CRIANDO CERTIIFICADO E CHAVE DE CLIENTE
CLIENTE="$PASTA"
# mkdir -p "$CLIENTE"
openssl genrsa -out "$CLIENTE/client.key" 2048
openssl req -new -key "$CLIENTE/client.key" -out "$CLIENTE/client.csr" -subj "/CN=client"
openssl x509 -req -in "$CLIENTE/client.csr" -CA "$PASTA/$DOMINIO.crt" -CAkey "$PASTA/$DOMINIO.key" -set_serial 01 -out "$CLIENTE/client.crt" -days 30 -sha256

rm "$PASTA"/*.csr
