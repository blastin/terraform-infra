const URL = process.env.URL;

exports.handler = async (event, _, callback) => {

    const path = event.path;

    const location = `${URL}${path}`;

    const response = {

        statusCode: 308,

        headers: {
            Location: location,
        }

    };

    return callback(null, response);

};
