# Terraform

Antes de provisionar os recursos, será necessário criar configurar um arquivo *terraform.tfvars*. Segue abaixo um exemplo de configuração

## Configuração Exemplo

```terraform
oidc_issuer        = "realms/knin-keycloak"
oidc_url           = "https://keycloak.com.br"
provider_name      = "keycloak"
api_gateway_name   = "api"
domain_name        = "doacao"
oidc_client_id     = "doacao_client"
oidc_client_secret = "3243234322"
app_client_subject = "CN"
header_subject     = "subject"
token_lambda_name  = "token"
auth_lambda_name   = "authorize"
aws_account_id     = "sa-east-1:0000342567678"
profile            = "default"
secret_manager_id  = "client_credentials/ID"
region             = "sa-east-1"
realm_lambda_name  = "realm"
```

## Provisionar

```sh
cd src
terraform init
cd ..
terraform -chdir="./src" plan -var-file="../env/terraform.tfvars" -out "plan"
terraform -chdir="./src" apply "plan"
```

## Destruir

```sh
    terraform -chdir="./src" destroy -var-file="../env/terraform.tfvars" -auto-approve
```

---

## Testar

Para recuperar um Token

```sh
export AWS_PROFILE="default"
ID="$(aws apigateway get-rest-apis --query "items[?name=='api'].id|[0]" | tr -d \")"
REGION="$(aws configure get region)"
HOST="https://$ID.execute-api.$REGION.amazonaws.com/v1"
curl --location --request POST "$HOST/token?subject=CN=client" \
--header 'subject: CN = client,ST = SP,C = BR'
```

Para Abrir um Fluxo de login

```sh
export AWS_PROFILE="default"
ID="$(aws apigateway get-rest-apis --query "items[?name=='api'].id|[0]" | tr -d \")"
REGION="$(aws configure get region)"
HOST="$ID.execute-api.$REGION.amazonaws.com/v1"
USER_POOL_ID="$(aws cognito-idp list-user-pools --query "UserPools[?Name=='knin-project-online'].Id |[0]" --max-results 20 | tr -d \")"
CLIENT_ID="$(aws cognito-idp list-user-pool-clients --user-pool-id $USER_POOL_ID --query "UserPoolClients[?ClientName=='authorize'].ClientId | [0]" | tr -d \")"
URL="$(echo "https://$HOST/authorize?client_id=$CLIENT_ID&redirect_uri=https://www.google.com")"
echo $URL
xdg-open "$URL"
```
